import { useState } from "react"
import Logo from "./otus_logo.svg";

export default function AuthorizationForm() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  return (
    <div className="auth_block">
      <img className="auth_logo" src={Logo}></img>
      <form className="auth_editors" action="#" method="post">
        <input
          className="auth_editor"
          type="email"
          name="auth_email"
          placeholder="Введите Ваш имейл"
          required
          onChange={(e) => setEmail(e.target.value)}
        />
        <input
          className="auth_editor"
          type="password"
          name="auth_pass"
          placeholder="Введите пароль"
          required
          onChange={(e) => setPassword(e.target.value)}
        />
        <button 
        className="auth_button" 
        name="form_auth_submit" 
        onClick={() => sendData({
            email, 
            password
        })}>
          Войти
        </button>
      </form>
    </div>
  );
}

const sendData = (data) => {
  fetch({
    type: "POST",
    url: "/api",
    body: JSON.stringify(data),
  });
};
